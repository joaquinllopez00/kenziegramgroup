import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import "antd/dist/antd.css";
import { Nav } from "./components/nav/Nav";
import { Homepage } from "./components/homepage/Homepage";
import { Photos } from "./components/photos/Photos";

function App() {
  return (
    <BrowserRouter>
      <div id="contentView">
        <Switch>
          <Route exact path="/" component={Homepage}>
            <Homepage />
          </Route>
          <Route path="/photos" render={(props) => <Photos />} />
        </Switch>
      </div>
      <Nav />
    </BrowserRouter>
  );
}

export default App;
