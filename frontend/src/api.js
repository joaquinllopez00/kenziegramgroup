import axios from "axios";

export const BASE_URL = "http://localhost:5000";

const axiosInstance = axios.create({
  baseURL: BASE_URL,
});

export default axiosInstance;

axiosInstance.get("/photos").then(({ data }) => {
  console.log({ data });
});

export const getPhotos = async () => {
  const { data } = await axiosInstance.get("/photos");
  return data;
};

export const getPhoto = async () => {
  const { data } = await axiosInstance.get("/photos/:photoId");

  return data;
};

export const uploadPic = async (data) => {
  const formData = new FormData();
  formData.append("photo", data);
  await axiosInstance.post("/photos", formData);
};
