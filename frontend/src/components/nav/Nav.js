import React from "react";
import { NavLink } from "react-router-dom";
import {CameraOutlined, UploadOutlined} from "@ant-design/icons";

import "./nav.css";

export const Nav = (props) => {
  return (
    <div id="nav">
      <h1> <CameraOutlined style={{fontSize:"1.25em"}}/> KenzieGram</h1>
      <div id="navLinks">
      <NavLink exact to="/">
        <UploadOutlined />
      </NavLink>
      <NavLink to="/photos">
        <CameraOutlined />
      </NavLink>
      </div>
    </div>
  );
};
