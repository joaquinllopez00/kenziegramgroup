import React, { Component } from "react";
import { Popover } from "antd";
import PopUpContent from "./PopUpContent/popupcontent";

export class Photos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
    this.hide = this.hide.bind(this);
    this.show = this.show.bind(this);
  }

  hide = () => {
    this.setState({
      visible: false,
    });
  };
  show = () => {
    this.setState({
      visible: !this.state.visible,
    });
  };

  render() {
    return (
      <Popover
        placement="right"
        content={<PopUpContent photoSrc={this.props.photoSrc} filesize={this.props.filesize} />}
        title="Additional Info"
        trigger="click"
        visible={this.state.visible}
        onVisibleChange={this.show}
      >
        <div className="photoContainer">
          <p className="desc"> {this.props.photoAlt}</p>
          <img className="photo" src={this.props.photoSrc} alt="" filesize={this.props.filesize} />
        </div>
      </Popover>
    );
  }
}
export default Photos;
