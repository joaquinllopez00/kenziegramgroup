import React, { Component } from "react";

export class PopUpContent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="popupContent">
        <img className="popupImage" alt=" Additional Info" src={this.props.photoSrc} />
        <p>Filesize: {this.props.filesize}</p>
        <p>(Click image again to close)</p>
      </div>
    );
  }
}
export default PopUpContent;
