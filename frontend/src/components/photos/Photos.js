import PhotoItem from "./photoItem/photoItem";
import "./photos.css";
import React from "react";
import { useEffect, useState } from "react";
import { getPhotos, BASE_URL } from "../../api";

const getFileSize = (file) => {
  for (let i = 0; i < file.length; i++) {
    if (file[i] === "_") {
      let fileSize = file.slice(8, i);

      if (Number(fileSize) > 999999) {
        fileSize = fileSize / 1000000;
        return toString(`${fileSize} MBs`);
      }
      let result = Math.ceil(fileSize / 1000);
      fileSize = result.toString();
      return `${fileSize} KBs`;
    }
  }
};

const getFileName = (file) => {
  for (let i = 0; i < file.length; i++) {
    let fileName;
    if (file[i] === "_") {
      return (fileName = file.slice(i + 1, file.length - 4));
    }
  }
};

export const Photos = (props) => {
  const [photos, setPhotos] = useState([]);
  const [serverUrl, setServerUrl] = useState("");
  useEffect(() => {
    async function fetchPhotos() {
      setPhotos(await getPhotos());
    }

    fetchPhotos();
    setServerUrl(BASE_URL);
  }, []);

  return (
    <div id="photosPage">
      {photos.map((photo, index) => {
        return (
          <PhotoItem
            key={index}
            photoSrc={`${serverUrl}/${photo}`}
            photoAlt={getFileName(photo)}
            filesize={getFileSize(photo)}
          />
        );
      })}
    </div>
  );
};

export default Photos;
