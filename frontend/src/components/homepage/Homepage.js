import { UploadOutlined } from "@ant-design/icons";
import "./homepage.css";
import { useState, useRef } from "react";
import Button from "react-bootstrap/Button";
import { uploadPic } from "../../api";

export const Homepage = (props) => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadStatus, setUploadStatus] = useState(null);
  const formRef = useRef();
  const submitForm = async (e) => {
    try {
      e.preventDefault();
      console.log("selectedFIle", selectedFile);
      if (findFileExt(selectedFile.name) === "jpg") {
        await uploadPic(selectedFile);
        setUploadStatus("Success!");
      } else {
        throw "File must be jpg format";
      }
    } catch (err) {
      console.log(err);
      setUploadStatus(err);
    } finally {
      setTimeout(() => {
        setUploadStatus(null);
      }, 3000);
    }
  };
  const findFileExt = (file) => {
    const fileExt = file.slice(file.length - 3, file.length);
    console.log(fileExt);
    return fileExt;
  };

  const onSelectFile = async (e) => {
    await setSelectedFile(e.target.files[0]);
  };
  return (
    <div className="homepage__container">
      
      {uploadStatus && <h1>{uploadStatus}</h1>}
      <div id="homePage">
        <form id="submitForm" ref={formRef} onSubmit={submitForm}>
          <label id="uploadLabel">
          
            <UploadOutlined />
            <input id="upload" type="file" name="photo" onChange={onSelectFile} />
          </label>
          <Button type="submit" className="homepage__submitbutton">
            Submit
          </Button>
        </form>
        
      </div>
      <p class="footNote"> * Note: If your file is larger than 2mb or not .jpg format you will be unable to upload file</p>
    </div>
  );
};
