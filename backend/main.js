// ESM syntax is supported.
//import express
import express from "express";
import "./multer";
import path from "path";
import { uploader, UPLOAD_DIRECTORY, getUploads, findUpload } from "./multer";
import cors from "cors";
import fs from "fs/promises";
//TODO give pics list

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/uploads", express.static(UPLOAD_DIRECTORY));
//tell what files we are serving

//TODO REQUESTS
app.get("/photos", async (req, res) => {
  try {
    const files = await getUploads();
    const fileData = files.map((file) => path.join(".", UPLOAD_DIRECTORY, file));
    if (!req.query.size) {
      res.json(fileData);
      return;
    }
    const size = Number.parseInt(req.query.size, 10);

    const results = [];
    for (let file of files) {
      const result = await findUpload(file);
      if (result.size <= size) {
        results.push(file);
      }
    }

    res.json(results);
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
});

app.get("/photos/:filename", async (req, res) => {
  try {
    const { birthtime, size } = await findUpload(req.params.filename);
    res.send({ birthtime, size });
  } catch (err) {
    res.status(404).json({
      message: "File not found :(",
    });
  }
});

app.post("/photos", uploader.single("photo"), (req, res) => {
  const size = req.file.size;

  try {
    if (size < 2000000) {
      res.json({
        photo: req.file.path,
      });
      return;
    }
    throw "File Size Too Large : File must be less than 2mb";
  } catch (err) {
    console.error(err);
  }
});

//listen

app.listen(5000, () => {
  console.log(`Listening on port 5000`);
});

//new test comment
//test
