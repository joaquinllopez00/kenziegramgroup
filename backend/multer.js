import multer from "multer";
import path from "path";
import { v4 } from "uuid";
import fs from "fs/promises";

export const UPLOAD_DIRECTORY = "./uploads";

const storage = multer.diskStorage({
  destination: async function fileDestination(req, file, callback) {
    try {
      await checkForDir();
      callback(null, UPLOAD_DIRECTORY);
    } catch (err) {
      callback(err);
    }
  },

  filename: async function (req, file, callback) {
    const fileExtension = path.extname(file.originalname);
    const fileSize = req.rawHeaders[5];
    const fileName = `${fileSize}_${file.originalname}`;
    try {
      if (fileExtension === ".jpg") {
        callback(null, fileName);
      } else {
        throw "The File you're attempting to upload is not of the .jpg format";
      }
    } catch (err) {
      return err;
    }
  },
});

const checkForDir = async () => {
  try {
    const dirExists = await fs.stat(UPLOAD_DIRECTORY);
  } catch (err) {
    if (err.code === "ENOENT") {
      await fs.mkdir(UPLOAD_DIRECTORY);
    }
  }
};

export const uploader = multer({ storage: storage });

export const getUploads = async () => {
  const files = await fs.readdir(UPLOAD_DIRECTORY);
  return files;
};

export const findUpload = async (fileName) => {
  const info = await fs.stat(path.resolve(UPLOAD_DIRECTORY, fileName));
  return info;
};
